package com.topriddy.learn;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class FunctionalInterfaceExample {
    public static void main(String args[]) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        //print out all numbers
        System.out.println("Print all the numbers: ");
        eval(numbers, n -> true);

        //print out all even numbers
        System.out.println("Print all the even numbers: ");
        eval(numbers, n -> n % 2 == 0);

        //print out all odd numbers
        System.out.println("Print all the odd numbers: ");
        eval(numbers, n -> n % 2 != 0);
    }

    public static void eval(List<Integer> numbers, Predicate<Integer> predicate) {
        numbers.forEach(System.out::println);
    }
}