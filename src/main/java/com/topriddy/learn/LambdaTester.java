package com.topriddy.learn;

public class LambdaTester {
    public static void main(String args[]) {
        LambdaTester tester = new LambdaTester();

        //with type declaration
        MathOperation addition = (int a, int b) -> a + b;

        //without type declaration
        MathOperation subtraction = (a, b) -> a - b;

        //with return statement along with curly braces
        MathOperation multiplication = (int a, int b) -> {
            return a * b;
        };

        //without return statement and curly braces
        MathOperation division = (int a, int b) -> (a / b);

        System.out.printf("10 + 5 =  %d \n", tester.operate(10, 5, addition));
        System.out.printf("10 - 5 =  %d \n", tester.operate(10, 5, subtraction));
        System.out.printf("10 * 5 =  %d \n", tester.operate(10, 5, multiplication));
        System.out.printf("10 / 5 =  %d \n", tester.operate(10, 5, division));
        System.out.printf("10 %% 5 =  %d \n", tester.operate(10, 5, (a, b) -> a % b));

        //with parenthesis
        GreetingService englishGreetings = (name) -> System.out.printf("Hello, %s \n", name);

        //without parenthesis
        GreetingService yorubaGreetings = name -> System.out.printf("Bawo ni, %s \n", name);

        englishGreetings.sayMessage("Haleemah");

        yorubaGreetings.sayMessage("Temitope");
    }

    interface MathOperation {
        int operation(int a, int b);
    }

    interface GreetingService {
        void sayMessage(String name);
    }

    private int operate(int a, int b, MathOperation mathOperation) {
        return mathOperation.operation(a, b);
    }

}