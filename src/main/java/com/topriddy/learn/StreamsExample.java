package com.topriddy.learn;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsExample {
    public static void main(String args[]) {
        List<String> names = Arrays.asList("Temitope", "Abiodun", "Muyiwa", "Sade", "Odunayo");
        String namesAsCsv = names.stream().collect(Collectors.joining(", "));
        System.out.println("namesAsCsv: " + namesAsCsv);

        String sortedAsCsv = names.stream().sorted().collect(Collectors.joining(", "));
        System.out.println("sortedAsCsv: " + sortedAsCsv);

    }

}
